﻿
class Progam {
    private static Random random = new Random();

    public static bool found=false;

    public static Dictionary<int, string> lookup1 = new Dictionary<int, string>(); 
    public static Dictionary<int, string> lookup2 = new Dictionary<int, string>();



    public static void Main(string[] args) {

        while (!found)
        {
            Task.Run(() => TryGetSameHashCode());
        }

    }

    public static void TryGetSameHashCode() 
    {
        if (found)
            return;
        string random = generateRandomString();
        int randomHash = random.GetHashCode();

        if (lookup1.TryGetValue(randomHash, out string stock) && !stock.Equals(random))
        {
                if (lookup2.TryGetValue(randomHash, out string stock2) && !stock2.Equals(stock) && !stock2.Equals(random))
                {
                    Console.WriteLine("MATCH FOUND!! {0} and {1} and {2} hash code {3}", lookup1[randomHash], lookup2[randomHash], random, randomHash);
                    found=true;
                    return;
                }
                lock(lookup2)
                    lookup2[randomHash] = random;
                if (lookup2.Count % 100 == 0)
                {
                    Console.WriteLine("lookup2 count: {0}", lookup2.Count);
                }
        }
        lock(lookup1)
            lookup1[randomHash] = random;

        return;
    }

    public static string generateRandomString() 
    {
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        return new string(Enumerable.Repeat(chars, 15)
            .Select(s => s[random.Next(s.Length)]).ToArray());

    }

}
